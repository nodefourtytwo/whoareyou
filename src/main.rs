#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

use std::io::prelude::*;
use std::fs::File;

fn get_hostname() -> String {
    let mut file = File::open("/etc/hostname").expect("Unable to open the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Unable to read the file");
    return contents;
}

fn get_mac_address() -> String {
    let mut contents = String::new();
    let mut file = match File::open("/sys/class/net/eth0/address") {
        Err(_why) => return contents,
        Ok(file) => file,
    };
    file.read_to_string(&mut contents).expect("Unable to read the file");
    return contents;
}

#[get("/")]
fn index() -> String {
    let result = format!("{}\n{}\n{}\n{}", 
        "# cat /etc/hostname",
        get_hostname(),
        "# cat /sys/class/net/eth0/address",
        get_mac_address()
    );
    return result;
}

fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .mount("/", routes![index])
}

fn main() {
    rocket().launch();
}
