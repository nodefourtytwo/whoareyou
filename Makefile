build:
	docker run --rm -it -v "$(shell pwd)":/home/rust/src ekidd/rust-musl-builder:nightly cargo build --release
	docker run --rm -it -v "$(shell pwd)":/home/rust/src ekidd/rust-musl-builder:nightly strip /home/rust/src/target/x86_64-unknown-linux-musl/release/whoareyou
	docker build -t nodefourtytwo/whoareyou .
