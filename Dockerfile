FROM scratch

WORKDIR /app/

EXPOSE 1337
ENV ROCKET_ENV="prod"

CMD ["/app/whoareyou"]

COPY Rocket.toml .
COPY target/x86_64-unknown-linux-musl/release/whoareyou .

