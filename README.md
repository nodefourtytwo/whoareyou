# Who are you?

A simple tool to send back the hostname and the mac adress of the server it's running on.

It can be used to test or demonstrate load-balancing.

## Build

```
make build
```

## Run

```
docker run -d -p 1337:1337 nodefourtytwo/whoareyou
```
